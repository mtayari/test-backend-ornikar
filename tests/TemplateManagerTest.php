<?php

namespace Tests;

use DateTime;
use App\Entity\Lesson;
use App\Entity\Learner;
use App\Entity\Template;
use App\TemplateManager;
use App\Entity\Instructor;
use App\Entity\MeetingPoint;
use PHPUnit\Framework\TestCase;
use App\Repository\LearnerRepository;
use App\Repository\InstructorRepository;
use App\Repository\MeetingPointRepository;
use App\TemplateManager\TemplateManagerLearner;
use App\TemplateManager\TemplateManagerInstructor;
use App\TemplateManager\TemplateManagerMeetingPoint;

class TemplateManagerTest extends TestCase
{
    protected function setUp(): void
    {
        InstructorRepository::getInstance()->save(new Instructor(1, 'Michael', 'Knight'));
        LearnerRepository::getInstance()->save(new Learner(1, 'Mel', 'Dupond', 'mel.dupond@gmail.com'));
        MeetingPointRepository::getInstance()->save(new MeetingPoint(1, '12 rue Charles Brunellière', 'Nantes'));
    }

    public function test_it_replaces_placeholders(): void
    {
        $template = new Template(
            subject: 'Votre leçon de conduite avec [instructor:firstname]',
            content: <<<CONTENT
            Bonjour [learner:firstname],

            La réservation du [lesson:start_date] de [lesson:start_time] à [lesson:end_time] avec [instructor:link] a bien été prise en compte !
            Voici votre point de rendez-vous: [meeting_point:address], [meeting_point:city].

            Bien cordialement,

            L'équipe Ornikar
        CONTENT);

        $result = (new TemplateManager(
            new TemplateManagerInstructor(InstructorRepository::getInstance()),
            new TemplateManagerLearner(LearnerRepository::getInstance()),
            new TemplateManagerMeetingPoint(MeetingPointRepository::getInstance())
        ))->compute($template, new Lesson(
                id:1,
                meetingPointId: 1,
                instructorId: 1,
                learnerId: 1,
                startTime: new DateTime('2021-01-01 12:00:00'),
                endTime: new DateTime('2021-01-01 13:00:00'),
            ));

        self::assertEquals('Votre leçon de conduite avec Michael', $result->subject);
        self::assertEquals(<<<CONTENT
            Bonjour Mel,

            La réservation du 01/01/2021 de 12:00 à 13:00 avec <a href="/instructors/1">Michael</a> a bien été prise en compte !
            Voici votre point de rendez-vous: 12 rue Charles Brunellière, Nantes.

            Bien cordialement,

            L'équipe Ornikar
        CONTENT, $result->content);
    }
}
