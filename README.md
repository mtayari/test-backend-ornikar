# PHP Hiring Test

## Context

**Ornikar** needs to send a lot of notification messages for different occasions.  
The `TemplateManager` class is dedicated to prepare the notifications by replacing the placeholders with the real values, so that they are ready to be sent.  

Your mission is to code this class, showing your exceptional skills to make a great clean implementation.

This exercise should not last longer than **30min**.

## Instructions

1. Open this project inside your favorite IDE
2. Inside a terminal, execute `make`
3. Code until `make test` passes!


#### Good luck!
