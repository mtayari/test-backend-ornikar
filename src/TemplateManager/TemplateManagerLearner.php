<?php

namespace App\TemplateManager;

use App\Entity\Lesson;
use App\Entity\Template;
use App\Repository\LearnerRepository;

class TemplateManagerLearner
{
    public function __construct(
        private LearnerRepository $learnerRepository
    ){}

    public function compute(Template $template, Lesson $lesson): Template
    {
        $learner = $this->learnerRepository->getById($lesson->learnerId);
        $replacements = array(
            Template::CONTENT_LEARNER_FIRSTNAME => $learner?->firstname,
            Template::CONTENT_START_DATE => $lesson->startTime->format('d/m/Y'),
            Template::CONTENT_START_TIME => $lesson->startTime->format('H:i'),
            Template::CONTENT_END_TIME => $lesson->endTime->format('H:i'),
        );
        $template->subject = str_replace(array_keys($replacements), array_values($replacements), $template->subject);
        $template->content = str_replace(array_keys($replacements), array_values($replacements), $template->content);
        return $template;

        
    }
}
