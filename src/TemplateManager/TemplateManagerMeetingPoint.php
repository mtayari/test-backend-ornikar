<?php

namespace App\TemplateManager;

use App\Entity\Lesson;
use App\Entity\Template;
use App\Repository\MeetingPointRepository;

class TemplateManagerMeetingPoint
{
    public function __construct(
        private MeetingPointRepository $meetingPointRepository
    ){}

    public function compute(Template $template, Lesson $lesson): Template
    {
        $meetingPoint = $this->meetingPointRepository->getById($lesson->meetingPointId);
        $replacements = array(
            Template::CONTENT_MEETING_ADDRESS => $meetingPoint?->address,
            Template::CONTENT_MEETING_CITY => $meetingPoint?->city
        );
        $template->subject = str_replace(array_keys($replacements), array_values($replacements), $template->subject);
        $template->content = str_replace(array_keys($replacements), array_values($replacements), $template->content);
        return $template;
    }
}
