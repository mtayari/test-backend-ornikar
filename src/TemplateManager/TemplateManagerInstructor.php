<?php

namespace App\TemplateManager;

use App\Entity\Lesson;
use App\Entity\Template;
use App\Repository\InstructorRepository;

class TemplateManagerInstructor
{
    public function __construct(
        private InstructorRepository $instructorRepository
    ){}

    public function compute(Template $template, Lesson $lesson): Template
    {
        $instructor = $this->instructorRepository->getById($lesson->instructorId);
        $replacements = array(
            Template::SUBJECT_INSTRUCTOR => $instructor?->firstname,
            Template::CONTENT_LINK => sprintf('<a href="/instructors/%d">%s</a>', $instructor->id, $instructor->firstname),
        );
        $template->subject = str_replace(array_keys($replacements), array_values($replacements), $template->subject);
        $template->content = str_replace(array_keys($replacements), array_values($replacements), $template->content);
        return $template;
    }
}
