<?php

namespace App;

use App\Entity\Lesson;
use App\Entity\Template;
use App\TemplateManager\TemplateManagerLearner;
use App\TemplateManager\TemplateManagerInstructor;
use App\TemplateManager\TemplateManagerMeetingPoint;

class TemplateManager
{
    public function __construct(
        private TemplateManagerInstructor $templateManagerInstructor,
        private TemplateManagerLearner $templateManagerLearner,
        private TemplateManagerMeetingPoint $templateManagerMeetingPoint
    ){}

    public function compute(Template $template, Lesson $lesson): Template
    {
        $template = $this->templateManagerInstructor->compute($template, $lesson);
        $template = $this->templateManagerLearner->compute($template, $lesson);
        $template = $this->templateManagerMeetingPoint->compute($template, $lesson);

        return $template;
    }
}


