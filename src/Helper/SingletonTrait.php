<?php

namespace App\Helper;

// DO NOT MODIFY THIS CLASS
trait SingletonTrait
{
    protected static $instance = null;

    public static function getInstance(): static
    {
        if (!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }
}
