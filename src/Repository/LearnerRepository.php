<?php

namespace App\Repository;

use App\Entity\Learner;
use App\Helper\SingletonTrait;

// DO NOT MODIFY THIS CLASS
class LearnerRepository
{
    use SingletonTrait;

    private array $items = [];

    public function getById(int $id): ?Learner
    {
        return $this->items[$id] ?? null;
    }

    public function save(Learner $learner): void
    {
        $this->items[$learner->id] = $learner;
    }
}
