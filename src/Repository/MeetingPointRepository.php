<?php

namespace App\Repository;

use App\Entity\MeetingPoint;
use App\Helper\SingletonTrait;

// DO NOT MODIFY THIS CLASS
class MeetingPointRepository
{
    use SingletonTrait;

    private array $items = [];

    public function getById(int $id): ?MeetingPoint
    {
        return $this->items[$id] ?? null;
    }

    public function save(MeetingPoint $meetingPoint): void
    {
        $this->items[$meetingPoint->id] = $meetingPoint;
    }
}
