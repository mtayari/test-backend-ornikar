<?php

namespace App\Repository;

use App\Entity\Instructor;
use App\Helper\SingletonTrait;

// DO NOT MODIFY THIS CLASS
class InstructorRepository
{
    use SingletonTrait;

    private array $items = [];

    public function getById(int $id): ?Instructor
    {
        return $this->items[$id] ?? null;
    }

    public function save(Instructor $instructor): void
    {
        $this->items[$instructor->id] = $instructor;
    }
}
