<?php

namespace App\Entity;

use DateTime;

class Lesson
{
    public function __construct(
        public readonly int $id,
        public readonly int $meetingPointId,
        public readonly int $instructorId,
        public readonly int $learnerId,
        public readonly DateTime $startTime,
        public readonly DateTime $endTime,
    ) {
    }
}
