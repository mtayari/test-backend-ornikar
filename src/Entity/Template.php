<?php

namespace App\Entity;

class Template
{
    CONST SUBJECT_INSTRUCTOR = '[instructor:firstname]';
    CONST CONTENT_LEARNER_FIRSTNAME = '[learner:firstname]';
    CONST CONTENT_START_DATE = '[lesson:start_date]';
    CONST CONTENT_START_TIME = '[lesson:start_time]';
    CONST CONTENT_END_TIME = '[lesson:end_time]';
    CONST CONTENT_LINK ='[instructor:link]';
    CONST CONTENT_MEETING_ADDRESS = '[meeting_point:address]';
    CONST CONTENT_MEETING_CITY = '[meeting_point:city]';

    public function __construct(
        public string $subject,
        public string $content,
    ) {
    }
}
