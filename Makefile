VERSION=8.1
IMAGE=thecodingmachine/php:$(VERSION)-v4-cli
PHP=docker run --rm -v ${PWD}:/usr/src/app $(IMAGE)

.PHONY: install test

install: vendor

vendor: composer.json composer.lock
	$(PHP) composer install

test: vendor
	$(PHP) phpunit --colors=always
